json.array!(@posts) do |post|
  json.extract! post, :id, :tit
  json.url post_url(post, format: :json)
end
